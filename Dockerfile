FROM python:3.8-slim

WORKDIR /app

COPY package*.json ./

COPY pyproject.toml poetry.lock /app/

RUN pip install poetry

RUN poetry install --no-dev

COPY . .

EXPOSE 5000

CMD ["poetry", "run", "python", "app.py"]
